const { group } = require("console")
const fs = require("fs")
const path = require("path")

const dataFileName = "data.json"


    // 1. Retrieve data for ids : [2, 13, 23].



function employee_callback(Ids, callback){

  fs.readFile(path.join(__dirname, dataFileName), (err, data)=>{
    if(err){
      callback(err)
    }
    else{
      let employee = Object.entries(JSON.parse(data))
      let result = employee[0][1].filter((element)=>{
        return Ids.includes(element.id)
      })
      callback(null, result)
      fs.writeFile(path.join(__dirname,"emp_1.json"), JSON.stringify(result), (err)=>{
        if(err){
          console.log(err)
        }
        else{
          group_companies(employee, callback)
        }
      })
      
    }
  })
}




    // 2. Group data based on companies.


    function group_companies(employee, callback) {
      
      let result = employee[0][1].reduce((acc, currValue)=>{
        if(!acc[currValue.company]){
          acc[currValue.company] = []
          acc[currValue.company].push(currValue)
        }
        else{
          if(!acc[currValue.company].includes(currValue)){
            acc[currValue.company].push(currValue)
          }
        }
        return acc
      },{})
      fs.writeFile(path.join(__dirname, "emp_2.json"), JSON.stringify(result), (err, data)=>{
        if(err){
          callback(err)
        }
        else{
          callback(data)
          dataForCompany(employee, callback)
        }
      })
      
    }



    //     { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}
    // 3. Get all data for company Powerpuff Brigade

function dataForCompany(params) {
  
}

    // 4. Remove entry with id 2.
    // 5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
    // 6. Swap position of companies with id 93 and id 92.
    // 7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.

function callback(err, data){
  if(err){
    console.log(err)
  }else{
    console.log(data)
  }
}


employee_callback([2,13,23],callback)